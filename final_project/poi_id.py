#!/usr/bin/env python

import sys
import pickle
from pprint import pprint
sys.path.append("../tools/")

from feature_format import featureFormat, targetFeatureSplit
from tester import dump_classifier_and_data

# Task 1: Select what features you'll use.
# features_list is a list of strings, each of which is a feature name.
# The first feature must be "poi".
features_list = [
    'poi',
    'bonus',
    # 'deferral_payments',
    # 'deferred_income',
    # 'director_fees',
    # 'exercised_stock_options',
    # 'from_poi_to_this_person',
    # 'from_this_person_to_poi',
    # 'loan_advances',
    # 'long_term_incentive',
    # 'restricted_stock',
    'salary',
    # 'total_stock_value',
    # 'shared_receipt_with_poi',

    'to_poi_ratio',
    'from_poi_ratio'
]  # You will need to use more features

# Load the dictionary containing the dataset
with open("final_project_dataset.pkl", "r") as data_file:
    data_dict = pickle.load(data_file)

data_dict.pop('TOTAL', 0)

for v in data_dict.itervalues():
    from_poi = v['from_poi_to_this_person']
    to_poi = v['from_this_person_to_poi']
    if from_poi == 'NaN' or to_poi == 'NaN':
        v['from_poi_ratio'] = 'NaN'
        v['to_poi_ratio'] = 'NaN'
    else:
        v['from_poi_ratio'] = float(from_poi) / v['to_messages']
        v['to_poi_ratio'] = float(to_poi) / v['from_messages']

# print 'data points', len(data_dict)
# print ''
# ljustname = 2 + max(map(len, data_dict.keys()))
# ljustsal = 2 + max(map(lambda i: len(str(i['salary'])), data_dict.values()))
# for k, v in data_dict.iteritems():
#     print '%s Salary: %s Bonus: %s' % (k.ljust(ljustname), str(v['salary']).ljust(ljustsal), v['bonus'])

# Task 2: Remove outliers
# Task 3: Create new feature(s)
# Store to my_dataset for easy export below.
my_dataset = data_dict

# Extract features and labels from dataset for local testing
data = featureFormat(my_dataset, features_list, sort_keys=True)
labels, features = targetFeatureSplit(data)

# Task 4: Try a variety of classifiers
# Please name your classifier clf for easy export below.
# Note that if you want to do PCA or other multi-stage operations,
# you'll need to use Pipelines. For more info:
# http://scikit-learn.org/stable/modules/pipeline.html

# Provided to give you a starting point. Try a variety of classifiers.
from sklearn.ensemble import RandomForestClassifier, AdaBoostClassifier
from sklearn.decomposition import PCA
from sklearn.pipeline import Pipeline
from sklearn.feature_selection import SelectFdr, SelectKBest
from sklearn.preprocessing import MinMaxScaler

# clf = RandomForestClassifier(n_estimators=10, min_samples_split=10, n_jobs=-1)

# Task 5: Tune your classifier to achieve better than .3 precision and recall
# using our testing script. Check the tester.py script in the final project
# folder for details on the evaluation method, especially the test_classifier
# function. Because of the small size of the dataset, the script uses
# stratified shuffle split cross validation. For more info:
# http://scikit-learn.org/stable/modules/generated/sklearn.cross_validation.StratifiedShuffleSplit.html

# Example starting point. Try investigating other evaluation techniques!
from sklearn.cross_validation import train_test_split
features_train, features_test, labels_train, labels_test = \
    train_test_split(features, labels, test_size=0.3, random_state=42)


estimators = [
    ('scale', MinMaxScaler()),
    ('reduce_dim', PCA(svd_solver='randomized', n_components=2)),
    # ('reduce_dim', SelectKBest(k=4)),
    # ('reduce_dim', SelectFdr()),
    ('clf', AdaBoostClassifier())
    # ('clf', RandomForestClassifier(n_estimators=20, min_samples_split=3, n_jobs=-1))
]
clf = Pipeline(estimators)
# clf = AdaBoostClassifier()

# kb = SelectKBest(k=3)
# kb.fit(features_train, labels_train)
# pprint(['%.2f' % i for i in kb.scores_])

# Task 6: Dump your classifier, dataset, and features_list so anyone can
# check your results. You do not need to change anything below, but make sure
# that the version of poi_id.py that you submit can be run on its own and
# generates the necessary .pkl files for validating your results.

dump_classifier_and_data(clf, my_dataset, features_list)
