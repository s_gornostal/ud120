#!/usr/bin/python


def outlierCleaner(predictions, ages, net_worths):
    """
        Clean away the 10% of points that have the largest
        residual errors (difference between the prediction
        and the actual net worth).

        Return a list of tuples named cleaned_data where
        each tuple is of the form (age, net_worth, error).
    """

    cleaned_data = []
    for pred, age, nw in zip(predictions, ages, net_worths):
        error = (nw - pred[0]) ** 2
        cleaned_data.append((age, nw, error))

    cleaned_data.sort(lambda a, b: -1 if a[2] < b[2] else 1)
    cleaned_data = cleaned_data[:81]

    return cleaned_data
